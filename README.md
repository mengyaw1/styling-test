# Styling Test

## Environment requirements:

- [Node.js](https://nodejs.org/en/)
- [npm](https://www.npmjs.com)
- [Webpack](https://webpack.github.io/)

## Installation:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install node openssl
npm install -g webpack
git clone git clone https://mengyaw1@bitbucket.org/mengyaw1/styling-test.git ~/folder/
cd ~/folder/styling-test
npm install
npm start
```

## Run locally as development, with HMR enabled:
`npm start`

## Run as production:
`npm run start:prod`

Note that this includes building, which takes a while

## Build for production:
`npm run build`

## Responsive breakpoints:
```
[
  {
    name: 'xxs',
    px: 359
  },
  {
    name: 'xs',
    px: 480
  },
  {
    name: 's',
    px: 640
  },
  {
    name: 'm',
    px: 768
  },
  {
    name: 'l',
    px: 1024
  },
  {
    name: 'xl',
    px: 1244
  },
  {
    name: 'xxl',
    px: 1410
  }
]
```

### Examples
```scss
@include bp(s)      { } // Small and up
@include bp(s, m)   { } // Between small and medium
@include bp(1024)   { } // 1024px and up
@include bp(0, xxs) { } // Between 0 and xxs
```